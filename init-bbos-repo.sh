#! /bin/zsh
# vim:noet:



REPODIR=/var/cache/pacman/bbos
REPODB=$REPODIR/bbos.db.tar

# pacman local repo and aurutils
sudo install -d $REPODIR -o $USER
cat | sudo tee -a /etc/pacman.d/bbos << EOF
[options]
CacheDir = /var/cache/pacman/pkg
CacheDir = /var/cache/pacman/bbos
CleanMethod = KeepCurrent
[bbos]
SigLevel = Optional TrustAll
Server = file:///var/cache/pacman/bbos
EOF
echo "Include = /etc/pacman.d/bbos" | sudo tee -a /etc/pacman.conf
sudo sed -i 's/#Color/Color/' /etc/pacman.conf

cd $HOME
#gpg --recv-keys DBE7D3DD8C81D58D0A13D0E76BC26A17B9B7018A
repo-add $REPODB
git clone https://gitlab.com/bitbuilder-cloud/bbos/bbos.git
cd bbos
makepkg -si
mv *.pkg.tar.zst $REPODIR
cd ..
#rm -rf bbos
repo-add $REPODB $REPODIR/*.pkg.tar.zst
