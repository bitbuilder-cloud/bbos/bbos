PROG=bbos

# completions
COMP=bash zsh fish


# use bash
SHELL=/usr/bin/bash

# set project VERSION to last tag name. If no tag exists, set it to v0.0.0
$(eval TAGS=$(shell git rev-list --tags))
ifdef TAGS
	VERSION=$(shell git describe --tags --abbrev=0)
else
	VERSION=v0.0.0
endif

.PHONY: all release
# setup the -ldflags option for go build
#LDFLAGS=-ldflags "-X main.Version=$(VERSION)"

# build all executable, man page and completions
all: bbos

.PHONY: all

#bbos-base:
#	cd "$srcdir/$pkgname-$pkgver/bbos-base"
#	makepkg -Sc

bbos:
	echo "Building bbos..."
	#cd "$(srcdir)/$(pkgname)-$(pkgver)"
	makepkg -SL -f

install:
	echo "TODO: make install"
	@install -Dm755 "$pkgdir/rootfs/etc/"
	@install -Dm755 "$pkgdir/rootfs/root/"
	@install -Dm755 "$pkgdir/rootfs/opt/"

# remove build results
clean:
	rm -fv ./*/*.pkg.tar.xz
	rm -fv ./*/*.pkg.tar.xz.sig
	rm -fv ./*/*.pkg.tar.zst
	rm -fv ./*/*.pkg.tar.zst.sig
	rm -fv ./*/*.src.tar.gz
	rm -fv ./*/*.log

# (1) adjust version in PKGBUILD and in man documentation to RELEASE, commit
#     and push changes
# (2) create an annotated tag with name RELEASE
# syntax: make release RELEASE=vX.Y.Z
release:
	@if ! [ -z $(RELEASE) ]; then \
		REL=$(RELEASE); \
		sed -i -e "s/pkgver=.*/pkgver=$${REL#v}/" ./PKGBUILD; \
		git commit -a -s -m "release $(RELEASE)"; \
		git push; \
		git tag -a $(RELEASE) -m "release $(RELEASE)"; \
		git push origin $(RELEASE); \
	fi
